import { computeMap, fillMines, isMine, valueAt, getHash, xoshiro128ss, cyrb128 } from './util.js?v=4'

const $ = s => document.querySelector(s)

const COLOR = [ '#4aa', '#aaf', '#afa', '#faa', '#faf', '#ffa', '#adf', '#888', '#8ab' ]

const board = $('main')
const SIZ = 24, MINES = 72
let map = {}, elmap = {}
let minelist = []
let cursors = {}
let gameID = crypto.randomUUID(), started = false, ws = undefined, hash = undefined, ending = false

function initGame() {
  minelist = fillMines(map, SIZ, MINES)
  computeMap(map, SIZ)
  startGame()
}

function endGame(delay) {
  if (ending)
    return
  ending = true
  console.log('end game triggered')

  setTimeout(() => {
    map = {}
    elmap = {}
    minelist = []
    board.innerHTML = ''
    console.log('creating new game')
    minelist = fillMines(map, SIZ, MINES, xoshiro128ss(...hash))
    computeMap(map, SIZ)
    console.log('starting new game')
    startGame()
    ending = false
  }, delay)
}

function startGame() {
  hash = getHash(map)
  let i = 0
  for (let y = 0; y < SIZ; y++) {
    const row = document.createElement('div')
    row.classList.add('row')
    for (let x = 0; x < SIZ; x++) {
      let clicked = false
      const square = document.createElement('div')
      elmap[`${x},${y}`] = square
      square.classList.add('square')

      if (i % 2 == 1)
        square.classList.add('square-light')

      square.vclick = () => {
        if (square.classList.contains('square-flag'))
          return

        if (clicked)
          return
        clicked = true

        const mine = isMine(map, x, y)
        if (mine) {
          square.classList.add('square-mine')

          if (!ending) {
            endGame(150 + 50 * minelist.length)
            for (let i = 0; i < minelist.length; i++) {
              setTimeout(() => elmap[minelist[i]].vclick(), 100 + 50 * i)
            }
          }
        } else {
          square.classList.add('square-seen')
          const val = valueAt(map, x, y)
          if (val > 0) {
            square.textContent = val
            square.style.color = COLOR[val]
          } else {
            square.textContent = ''
            setTimeout(() => {
              for (let dy = -1; dy < 2; dy++)
                for (let dx = -1; dx < 2; dx++)
                  valueAt(elmap, x + dx, y + dy) && valueAt(elmap, x + dx, y + dy).vclick()
            }, 100)
          }
        }
      }
      square.addEventListener('click', () => {
        if (ending)
          return

        square.vclick()
        ws.send(JSON.stringify({
          event: `@narasminesweeper/${gameID}/click`,
          coord: `${x},${y}`
        }))
      })

      square.vflag = () => {
        if (clicked)
          return

        square.classList.toggle('square-flag')
      }
      square.addEventListener('contextmenu', ev => {
        ev.preventDefault()

        if (ending)
          return

        square.vflag()
        ws.send(JSON.stringify({
          event: `@narasminesweeper/${gameID}/flag`,
          coord: `${x},${y}`
        }))
      })
      i++
      row.appendChild(square)
    }
    if (SIZ % 2 == 0)
      i++
    board.appendChild(row)
  }
}

function connect() {
  ws = new WebSocket('wss://relay.jdevstudios.es/')
  ws.addEventListener('open', () => {
    ws.send(JSON.stringify({
      clientID: crypto.randomUUID(),
      listens: [
        `@narasminesweeper/${gameID}/join`,
        `@narasminesweeper/${gameID}/board`,
        `@narasminesweeper/${gameID}/click`,
        `@narasminesweeper/${gameID}/flag`,
        `@narasminesweeper/${gameID}/pointer`,
      ]
    }))

    console.log('attempting to join')
    ws.send(JSON.stringify({
      event: `@narasminesweeper/${gameID}/join`
    }))
    setTimeout(() => {
      if (!started) {
        console.log('could not join, hosting')
        started = true
        initGame()
      }
    }, 350)
  })

  // https://stackoverflow.com/a/63927372
  const cleaner = (key, value) => key === '__proto__' ? undefined : value

  ws.addEventListener('message', ({ data }) => {
    const pkt = JSON.parse(data)

    if (!started) {
      if (pkt.event.endsWith('/board')) {
        console.log('joined')
        map = pkt.map
        minelist = pkt.minelist
        started = true
        startGame()
      }
      return
    }

    if (pkt.event.endsWith('/join')) {
      console.log('user joined')
      ws.send(JSON.stringify({
        event: `@narasminesweeper/${gameID}/board`,
        map, minelist
      }))
    } else if (pkt.event.endsWith('/click')) {
      elmap[pkt.coord] && elmap[pkt.coord].vclick()
    } else if (pkt.event.endsWith('/flag')) {
      elmap[pkt.coord] && elmap[pkt.coord].vflag()
    } else if (pkt.event.endsWith('/pointer')) {
      if (!(pkt.$sender in cursors)) {
        const el = document.createElement('div')
        el.classList.add('cursor')

        const rng = xoshiro128ss(...cyrb128(pkt.$sender))
        const col = COLOR[Math.floor(rng() * COLOR.length)].substring(1)
        const [r, g, b] = [
          parseInt(col[0] + col[0], 16),
          parseInt(col[1] + col[1], 16),
          parseInt(col[2] + col[2], 16),
        ]
        el.style.background = `rgba(${r}, ${g}, ${b}, 0.5)`

        document.body.appendChild(el)
        cursors[pkt.$sender] = el
        cursors[pkt.$sender]._clear = setTimeout(()=>0)
      }

      cursors[pkt.$sender].style.left = pkt.x + 'px'
      cursors[pkt.$sender].style.top = pkt.y + 'px'

      clearTimeout(cursors[pkt.$sender]._clear)
      cursors[pkt.$sender]._clear = setTimeout(() => {
        cursors[pkt.$sender].remove()
        delete cursors[pkt.$sender]
      }, 500)
    }
  })
  ws.addEventListener('close', () => {
    // TODO show disconnection
  })

  let ccx = 0, ccy = 0, ccn = 0
  window.addEventListener('mousemove', ev => {
    if (!started)
      return

    ccx = ev.x
    ccy = ev.y
    ccn++

    if (ccn > 4) {
      ccn = 0
      ws.send(JSON.stringify({
        event: `@narasminesweeper/${gameID}/pointer`,
        x: ev.x, y: ev.y
      }))
    }
  })

  setInterval(() => {
    ws.send(JSON.stringify({
      event: `@narasminesweeper/${gameID}/pointer`,
      x: ccx, y: ccy
    }))
  }, 300)
}

(function() {
  const urlp = new URLSearchParams(location.search)
  if (!urlp.has('id')) {
    urlp.set('id', crypto.randomUUID())
    location.search = `?${urlp}`
    return
  }
  gameID = urlp.get('id')

  connect()
})()
