export function fillMines(map, size, count, rng=Math.random) {

  const mines = []
  while (count > 0) {
    const x = Math.floor(rng() * size)
    const y = Math.floor(rng() * size)
    if (`${x},${y}` in map)
      continue

    mines.push(`${x},${y}`)
    map[`${x},${y}`] = 0xFF
    count--
  }
  return mines
}

export function valueAt(map, x, y) {
  return map[`${x},${y}`] ?? 0
}
export function isMine(map, x, y) {
  return map[`${x},${y}`] == 0xFF
}

export function computeMap(map, size) {
  for (let y = 0; y < size; y++)
    for (let x = 0; x < size; x++) {
      if (isMine(map, x, y))
        continue

      let count = 0
      count += isMine(map, x, y - 1)
      count += isMine(map, x, y + 1)
      count += isMine(map, x - 1, y)
      count += isMine(map, x - 1, y - 1)
      count += isMine(map, x - 1, y + 1)
      count += isMine(map, x + 1, y)
      count += isMine(map, x + 1, y - 1)
      count += isMine(map, x + 1, y + 1)

      map[`${x},${y}`] = count
    }
}

// https://stackoverflow.com/a/47593316
export function cyrb128(str) {
  let h1 = 1779033703, h2 = 3144134277,
    h3 = 1013904242, h4 = 2773480762
  for (let i = 0, k; i < str.length; i++) {
    k = str.charCodeAt(i)
    h1 = h2 ^ Math.imul(h1 ^ k, 597399067)
    h2 = h3 ^ Math.imul(h2 ^ k, 2869860233)
    h3 = h4 ^ Math.imul(h3 ^ k, 951274213)
    h4 = h1 ^ Math.imul(h4 ^ k, 2716044179)
  }
  h1 = Math.imul(h3 ^ (h1 >>> 18), 597399067)
  h2 = Math.imul(h4 ^ (h2 >>> 22), 2869860233)
  h3 = Math.imul(h1 ^ (h3 >>> 17), 951274213)
  h4 = Math.imul(h2 ^ (h4 >>> 19), 2716044179)
  h1 ^= (h2 ^ h3 ^ h4), h2 ^= h1, h3 ^= h1, h4 ^= h1
  return [ h1>>>0, h2>>>0, h3>>>0, h4>>>0 ]
}
export function xoshiro128ss(a, b, c, d) {
  return () => {
    let t = b << 9, r = b * 5; r = (r << 7 | r >>> 25) * 9
    c ^= a; d ^= b
    b ^= c; a ^= d; c ^= t
    d = d << 11 | d >>> 21
    return (r >>> 0) / 4294967296
  }
}

export function getHash(map) {
  return cyrb128(Object.entries(map).map(x => `[${x}]`).toString())
}
